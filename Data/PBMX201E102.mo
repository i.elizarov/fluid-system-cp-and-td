within Data;
record PBMX201E102 "Performance record for the EDUR PBMX 201 E10.2 pump"

  extends IBPSA.Fluid.Movers.Data.Generic(
  
  speed_rpm_nominal=2900,
  use_powerCharacteristic = true,
    
  power(V_flow={2.777777777777778e-05,0.0009722222222222222}, P={4100, 2300}),
  pressure(V_flow={2.777777777777778e-05, 0.0009722222222222222}, dp={1181000, 319000}));

end PBMX201E102;
