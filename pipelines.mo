model pipelines "Model of the pipelines between the cooling plant and the inlet ports of the device under test (DUT)"

  package Medium =
      Novec649;


  Modelica.Fluid.Pipes.StaticPipe pipe_T1_1(redeclare package Medium = Medium, diameter = 35e-3, length = 0.8 + 0.8 + 9.3 + 3.63 + 0.35 + 0.65 + 1.93 + 0.735 + 0.5, roughness = 1.5e-06)  annotation(
    Placement(visible = true, transformation(origin = {-60, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Pipes.StaticPipe pipe_T1_2(redeclare package Medium = Medium, diameter = 28e-3, length = 0.656 + 0.572 + 0.190 + 0.178 + 1.629 + 0.85 + 0.69 + 0.2, roughness = 1.5e-06)  annotation(
    Placement(visible = true, transformation(origin = {-2, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Pipes.StaticPipe pipe_T1_3(redeclare package Medium = Medium, diameter = 33.7e-3, length = 4)  annotation(
    Placement(visible = true, transformation(origin = {58, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Pipes.StaticPipe pipe_T2_1(redeclare package Medium = Medium, diameter = 35e-3, length = 0.8 + 0.8 + 9.3 + 3.63 + 0.35 + 0.65 + 1.93 + 0.735 + 0.5, roughness = 1.5e-06) annotation(
    Placement(visible = true, transformation(origin = {-62, -26}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Fluid.Pipes.StaticPipe pipe_T2_2(redeclare package Medium = Medium, diameter = 28e-3, length = 0.656 + 0.572 + 0.190 + 0.178 + 1.629 + 0.85 + 0.69 + 0.2, roughness = 1.5e-06) annotation(
    Placement(visible = true, transformation(origin = {-2, -26}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Fluid.Pipes.StaticPipe pipe_T2_3(redeclare package Medium = Medium, diameter = 33.7e-3, length = 4) annotation(
    Placement(visible = true, transformation(origin = {56, -26}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  inner Modelica.Fluid.System system(p_start(displayUnit = "Pa") = 3e5)  annotation(
    Placement(visible = true, transformation(origin = {90, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_a port_T1_a(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-100, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-98, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_b port_T1_b(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {100, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_a port_T2_a(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {100, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-100, -26}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(port_T1_a, pipe_T1_1.port_a) annotation(
    Line(points = {{-100, 50}, {-88, 50}, {-88, 20}, {-70, 20}}));
  connect(pipe_T1_1.port_b, pipe_T1_2.port_a) annotation(
    Line(points = {{-50, 20}, {-12, 20}}, color = {0, 127, 255}));
  connect(pipe_T1_2.port_b, pipe_T1_3.port_a) annotation(
    Line(points = {{8, 20}, {48, 20}}, color = {0, 127, 255}));
  connect(pipe_T1_3.port_b, port_T1_b) annotation(
    Line(points = {{68, 20}, {86, 20}, {86, 50}, {100, 50}}, color = {0, 127, 255}));
  connect(pipe_T2_1.port_a, pipe_T2_2.port_b) annotation(
    Line(points = {{-52, -26}, {-12, -26}}, color = {0, 127, 255}));
  connect(pipe_T2_2.port_a, pipe_T2_3.port_b) annotation(
    Line(points = {{8, -26}, {46, -26}}));
  connect(pipe_T2_3.port_a, port_T2_a) annotation(
    Line(points = {{66, -26}, {80, -26}, {80, -50}, {100, -50}}, color = {0, 127, 255}));
  connect(port_b, pipe_T2_1.port_b) annotation(
    Line(points = {{-100, -26}, {-72, -26}}));
  annotation(
    uses(Modelica(version = "3.2.3")),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Rectangle(origin = {-3, 50}, extent = {{-45, 10}, {45, -10}}), Rectangle(origin = {-3, -46}, extent = {{-45, 10}, {45, -10}}), Line(origin = {54, 50}, points = {{-12, 0}, {12, 0}}), Line(origin = {-59, 50}, points = {{11, 0}, {-11, 0}}), Line(origin = {54, -46}, points = {{-12, 0}, {12, 0}}), Line(origin = {-61, -46}, points = {{13, 0}, {-13, 0}})}),
  Diagram(graphics = {Text(origin = {-60, 35}, extent = {{-16, 5}, {16, -5}}, textString = "Copper 35 mm"), Text(origin = {-2, 35}, extent = {{-16, 5}, {16, -5}}, textString = "Copper 28 mm"), Text(origin = {60, 36}, extent = {{-22, 8}, {22, -8}}, textString = "Stainless Steel 33.7 mm")}));
end pipelines;
