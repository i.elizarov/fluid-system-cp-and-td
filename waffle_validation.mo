model waffle_validation

  package Medium =
      Novec649;
  inner Modelica.Fluid.System system(m_flow_start = 0.008)  annotation(
    Placement(visible = true, transformation(origin = {-70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Sources.FixedBoundary boundary(redeclare package Medium = Medium, T = 273.15 - 40, nPorts = 1, p = 2e5)  annotation(
    Placement(visible = true, transformation(origin = {-44, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  Modelica.Fluid.Sources.FixedBoundary boundary1(redeclare package Medium = Medium, nPorts = 1, p = 1e5) annotation(
    Placement(visible = true, transformation(origin = {-44, -34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  waffle waffle_test(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {0, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(boundary.ports[1], waffle_test.port_a) annotation(
    Line(points = {{-34, 32}, {0, 32}, {0, 14}}, color = {0, 127, 255}));
  connect(waffle_test.port_b, boundary1.ports[1]) annotation(
    Line(points = {{0, -6}, {0, -34}, {-34, -34}}, color = {0, 127, 255}));
  annotation(
    uses(Modelica(version = "3.2.3")),
    Diagram);
end waffle_validation;
