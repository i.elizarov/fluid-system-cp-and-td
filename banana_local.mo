model banana_local

  package Medium =
      Modelica.Media.Water.ConstantPropertyLiquidWater;
  
  Piping_local piping(channel_number = 22)  annotation(
    Placement(visible = true, transformation(origin = {-6, -4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  inner Modelica.Fluid.System system(m_flow_start = 0.008)  annotation(
    Placement(visible = true, transformation(origin = {-70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Sources.FixedBoundary boundary(redeclare package Medium = Medium, nPorts = 4, p = 2e5)  annotation(
    Placement(visible = true, transformation(origin = {-44, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Sources.FixedBoundary boundary1(redeclare package Medium = Medium, nPorts = 4, p = 1e5)  annotation(
    Placement(visible = true, transformation(origin = {-44, -34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(boundary.ports[1], piping.port_a1) annotation(
    Line(points = {{-34, 32}, {-14, 32}, {-14, 6}}, color = {0, 127, 255}));
  connect(piping.port_a2, boundary.ports[2]) annotation(
    Line(points = {{-8, 6}, {-8, 32}, {-34, 32}}, color = {0, 127, 255}));
  connect(piping.port_a3, boundary.ports[3]) annotation(
    Line(points = {{-2, 6}, {-2, 32}, {-34, 32}}, color = {0, 127, 255}));
  connect(piping.port_a4, boundary.ports[4]) annotation(
    Line(points = {{2, 6}, {2, 32}, {-34, 32}}, color = {0, 127, 255}));
  connect(boundary1.ports[1], piping.port_b1) annotation(
    Line(points = {{-34, -34}, {-14, -34}, {-14, -14}}, color = {0, 127, 255}));
  connect(piping.port_b2, boundary1.ports[2]) annotation(
    Line(points = {{-10, -14}, {-10, -34}, {-34, -34}}, color = {0, 127, 255}));
  connect(piping.port_b3, boundary1.ports[3]) annotation(
    Line(points = {{-2, -14}, {-2, -34}, {-34, -34}}, color = {0, 127, 255}));
  connect(piping.port_b4, boundary1.ports[4]) annotation(
    Line(points = {{2, -14}, {2, -34}, {-34, -34}}, color = {0, 127, 255}));
  annotation(
    uses(Modelica(version = "3.2.3")),
    Diagram(graphics = {Text(origin = {41, 70}, extent = {{-39, 12}, {39, -12}}, textString = "Piping_local")}));
end banana_local;
