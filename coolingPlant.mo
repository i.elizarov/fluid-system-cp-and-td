model coolingPlant "Model of the cooling plant"

  package Medium =
      Novec649;

  parameter Modelica.SIunits.HeatFlowRate Q_max = 10.83e3 "Maximum cooling capacity of the plant";
    
  IBPSA.Fluid.Movers.SpeedControlled_y pump(
    redeclare package Medium = Medium,
    redeclare Data.PBMX201E102 per, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, p_start = 3e5
  
  ) annotation(
    Placement(visible = true, transformation(origin = {58, 54}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  
  IBPSA.Fluid.Sources.Boundary_pT bou(redeclare package Medium = Medium, T = 273.15 - 40, nPorts = 1, p = 3e5)  annotation(
    Placement(visible = true, transformation(origin = {-72, 54}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  inner Modelica.Fluid.System system annotation(
    Placement(visible = true, transformation(origin = {90, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  IBPSA.Fluid.Actuators.Valves.ThreeWayEqualPercentageLinear val(redeclare package Medium = Medium, CvData = IBPSA.Fluid.Types.CvTypes.Kv, Kv = 12, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, l = {0.02e-3, 0.2e-3}, m_flow_nominal = 1.125, p_start = 3e5, portFlowDirection_1 = Modelica.Fluid.Types.PortFlowDirection.Entering, portFlowDirection_2 = Modelica.Fluid.Types.PortFlowDirection.Leaving, portFlowDirection_3 = Modelica.Fluid.Types.PortFlowDirection.Leaving, riseTime = 15, use_inputFilter = true, y_start = 0.5)  annotation(
    Placement(visible = true, transformation(origin = {6, -26}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
  IBPSA.Fluid.Sensors.VolumeFlowRate volJun(redeclare package Medium = Medium, allowFlowReversal = false, m_flow_nominal = 1.125 * 0.5)  annotation(
    Placement(visible = true, transformation(origin = {68, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  IBPSA.Fluid.Sensors.VolumeFlowRate volDUT(redeclare package Medium = Medium, allowFlowReversal = false, m_flow_nominal = 1.125 * 0.5)  annotation(
    Placement(visible = true, transformation(origin = {146, 36}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  IBPSA.Fluid.FixedResistances.PressureDrop filter(redeclare package Medium = Medium, dp_nominal = 0.07e5, m_flow_nominal = 1.5854) annotation(
    Placement(visible = true, transformation(origin = {10, 54}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  IBPSA.Fluid.HeatExchangers.SensibleCooler_T coo(redeclare package Medium = Medium, QMin_flow = -Q_max, dp_nominal = 7.94e3, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, m_flow_nominal = 4050 / 3600, tau = 40) annotation(
    Placement(visible = true, transformation(origin = {-36, 10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder(T = 287.61) annotation(
    Placement(visible = true, transformation(origin = {-70, 28}, extent = {{-8, -8}, {8, 8}}, rotation = 0)));
equation
  connect(val.port_3, volJun.port_a) annotation(
    Line(points = {{6, -16}, {6, -9}, {68, -9}, {68, 0}}, color = {0, 127, 255}));
  connect(pump.port_a, volJun.port_b) annotation(
    Line(points = {{68, 54}, {68, 20}}, color = {0, 127, 255}));
  connect(volDUT.port_b, pump.port_a) annotation(
    Line(points = {{146, 46}, {146, 54}, {68, 54}}, color = {0, 127, 255}));
  connect(pump.port_b, filter.port_a) annotation(
    Line(points = {{48, 54}, {20, 54}}, color = {0, 127, 255}));
  connect(bou.ports[1], filter.port_b) annotation(
    Line(points = {{-62, 54}, {-46, 54}, {-46, 72}, {0, 72}, {0, 54}}, color = {0, 127, 255}));
  connect(coo.port_a, filter.port_b) annotation(
    Line(points = {{-36, 20}, {-36, 54}, {0, 54}}, color = {0, 127, 255}));
  connect(coo.port_b, val.port_1) annotation(
    Line(points = {{-36, 0}, {-36, -26}, {-4, -26}}, color = {0, 127, 255}));
  connect(firstOrder.y, coo.TSet) annotation(
    Line(points = {{-62, 28}, {-28, 28}, {-28, 22}}, color = {0, 0, 127}));
  annotation(
    uses(Modelica(version = "3.2.3"), IBPSA(version = "3.0.0")),
    Diagram);
end coolingPlant;
