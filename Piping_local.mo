model Piping_local "Sections of channels"

replaceable package Medium =
      Novec649;

parameter Modelica.SIunits.Height channel_height = 40e-3 "Height of the channels (CH)";

parameter Modelica.SIunits.Area channel_cross_section_area = 4.6e-6 "Cross-section area of channels";
parameter Modelica.SIunits.Length channel_cross_section_perimeter = 15.4e-3 "Perimeter of the channels";

parameter Modelica.SIunits.Height channel_roughness = 0.015e-3 "Rougness of wet surface";
parameter Integer channel_number = 22 "Vertical array of channels";

parameter Real epsilon_left_right(min = 0)= 0.5 "Local hydraulic resistance coefficient";
parameter Real epsilon_left_right_middle(min = 0)= 0.5 "Local hydraulic resistance coefficient";

  Modelica.Fluid.Pipes.StaticPipe pipe_left[channel_number](each allowFlowReversal = false, each crossArea = channel_cross_section_area,
    each final isCircular = false,
    each length = channel_height,
    each roughness = channel_roughness,
    each perimeter = channel_cross_section_perimeter,
    redeclare package Medium=Medium)  annotation(
    Placement(visible = true, transformation(origin = {-40, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe_middle_left[channel_number](each allowFlowReversal = false, each crossArea = channel_cross_section_area,
    each final isCircular = false,
    each length = channel_height,
    each roughness = channel_roughness,
    each perimeter = channel_cross_section_perimeter,
    redeclare package Medium=Medium) annotation(
    Placement(visible = true, transformation(origin = {-12, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    
  Modelica.Fluid.Pipes.StaticPipe pipe_middle_right[channel_number](each allowFlowReversal = false, each crossArea = channel_cross_section_area,
    each final isCircular = false,
    each length = channel_height,
    each roughness = channel_roughness,
    each perimeter = channel_cross_section_perimeter,
    redeclare package Medium=Medium) annotation(
    Placement(visible = true, transformation(origin = {18, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    
  Modelica.Fluid.Pipes.StaticPipe pipe_right[channel_number](each allowFlowReversal = false, each crossArea = channel_cross_section_area,
    each final isCircular = false,
    each length = channel_height,
    each roughness = channel_roughness,
    each perimeter = channel_cross_section_perimeter,
    redeclare package Medium=Medium) annotation(
    Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  
  inner Modelica.Fluid.System system(m_flow_small = 0.008e-3, m_flow_start = 0.008)  annotation(
    Placement(visible = true, transformation(origin = {130, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

  Modelica.Fluid.Interfaces.FluidPort_a port_a1(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-80, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-74, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_a port_a2(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-30, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-20, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_a port_a3(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {30, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {36, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_a port_a4(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {80, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {82, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    
    
  Modelica.Fluid.Interfaces.FluidPort_b port_b1(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-82, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-82, -98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_b port_b2(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-30, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-32, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_b port_b3(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {30, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {38, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_b port_b4(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {80, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {80, -98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

 Modelica.Fluid.Fittings.SimpleGenericOrifice loc_res_left[channel_number-1](each diameter = sqrt(4 * channel_cross_section_area / 3.14), each zeta = epsilon_left_right, redeclare package Medium = Medium)  annotation(
    Placement(visible = true, transformation(origin = {-50, -30}, extent = {{-8, -8}, {8, 8}}, rotation = -90)));

 Modelica.Fluid.Fittings.SimpleGenericOrifice loc_res_middle_left[channel_number-1](each diameter = sqrt(4 * channel_cross_section_area / 3.14), each zeta = epsilon_left_right_middle, redeclare package Medium = Medium)  annotation(
    Placement(visible = true, transformation(origin = {-24, -30}, extent = {{-8, -8}, {8, 8}}, rotation = -90)));
    
 Modelica.Fluid.Fittings.SimpleGenericOrifice loc_res_middle_right[channel_number-1](each diameter = sqrt(4 * channel_cross_section_area / 3.14), each zeta = epsilon_left_right_middle, redeclare package Medium = Medium)  annotation(
    Placement(visible = true, transformation(origin = {32, -30}, extent = {{-8, -8}, {8, 8}}, rotation = -90)));

  Modelica.Fluid.Fittings.SimpleGenericOrifice loc_res_right[channel_number-1](each diameter = sqrt(4 * channel_cross_section_area / 3.14), each zeta = epsilon_left_right, redeclare package Medium = Medium)  annotation(
    Placement(visible = true, transformation(origin = {68, -28}, extent = {{-8, -8}, {8, 8}}, rotation = -90)));
equation
  for i in 1:channel_number - 1 loop
    if Odd_even(i) then
    
      connect(pipe_left[i].port_b, loc_res_left[i].port_a);
      connect(loc_res_left[i].port_b, pipe_left[i + 1].port_a);

      connect(pipe_middle_left[i].port_b, loc_res_middle_left[i].port_a);
      connect(loc_res_middle_left[i].port_b, pipe_middle_left[i + 1].port_a);   
      
      connect(pipe_middle_right[i].port_b, loc_res_middle_right[i].port_a);
      connect(loc_res_middle_right[i].port_b, pipe_middle_right[i+1].port_a);
      
      connect(pipe_right[i].port_b, loc_res_right[i].port_a);
      connect(loc_res_right[i].port_b, pipe_right[i+1].port_a);    
      
    else
    
      connect(pipe_left[i].port_b, loc_res_left[i].port_a);
      connect(loc_res_left[i].port_b, pipe_left[i + 1].port_a);

      connect(pipe_middle_left[i].port_b, loc_res_middle_left[i].port_a);
      connect(loc_res_middle_left[i].port_b, pipe_middle_left[i + 1].port_a);   
      
      connect(pipe_middle_right[i].port_b, loc_res_middle_right[i].port_a);
      connect(loc_res_middle_right[i].port_b, pipe_middle_right[i+1].port_a);
      
      connect(pipe_right[i].port_b, loc_res_right[i].port_a);
      connect(loc_res_right[i].port_b, pipe_right[i+1].port_a); 
      
    end if;
  end for;
 connect(pipe_left[channel_number].port_b, port_b1) annotation(
    Line(points = {{-40, -10}, {-40, -56}, {-82, -56}, {-82, -100}}, color = {0, 127, 255}));
 connect(pipe_middle_left[channel_number].port_b, port_b2) annotation(
    Line(points = {{-12, -10}, {-14, -10}, {-14, -70}, {-30, -70}, {-30, -100}}, color = {0, 127, 255}));
 connect(pipe_middle_right[channel_number].port_b, port_b3) annotation(
    Line(points = {{18, -10}, {18, -74}, {30, -74}, {30, -100}}, color = {0, 127, 255}));
 connect(pipe_right[channel_number].port_b, port_b4) annotation(
    Line(points = {{50, -10}, {50, -62}, {80, -62}, {80, -100}}, color = {0, 127, 255}));
 connect(port_a1, pipe_left[1].port_a) annotation(
    Line(points = {{-80, 100}, {-80, 28}, {-40, 28}, {-40, 10}}));
 connect(port_a2, pipe_middle_left[1].port_a) annotation(
    Line(points = {{-30, 100}, {-30, 46}, {-12, 46}, {-12, 10}}));
 connect(port_a3, pipe_middle_right[1].port_a) annotation(
    Line(points = {{30, 100}, {30, 56}, {18, 56}, {18, 10}}));
 connect(port_a4, pipe_right[1].port_a) annotation(
    Line(points = {{80, 100}, {80, 40}, {50, 40}, {50, 10}}));
  annotation(
    uses(Modelica(version = "3.2.3")),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Line(origin = {-82, 1}, points = {{0, 43}, {0, -43}}), Line(origin = {-68, 2}, points = {{0, 44}, {0, -44}}), Line(origin = {-26, 5}, points = {{0, 45}, {0, -45}}), Line(origin = {-12, 6}, points = {{0, 46}, {0, -46}}), Line(origin = {30, 9}, points = {{0, 49}, {0, -49}}), Line(origin = {44, 10}, points = {{0, 50}, {0, -50}}), Line(origin = {76, 12}, points = {{0, 52}, {0, -52}}), Line(origin = {88, 13}, points = {{0, 53}, {0, -53}})}),
  Diagram(graphics = {Text(origin = {-82, 1}, extent = {{-12, 7}, {12, -7}}, textString = "N elements"), Text(origin = {-81, -30}, extent = {{-13, 8}, {13, -8}}, textString = "N-1 elements")}));
end Piping_local;
