model thermal_demonstrator "Thermohydraulic model of the thermal demonstrator"
  replaceable package Medium = Novec649 annotation(
    Documentation(info = "<html><head></head><body>This model does not include thermal performance of the side panel</body></html>"));
  parameter Modelica.SIunits.MassFlowRate m_flow_plate = 0.0893455 "Nominal flow rate for the cooling plate";
  parameter Modelica.SIunits.PressureDifference dp_plate = 1.32e5 "Pressure loss at the cooling plate";
  parameter Modelica.SIunits.Volume V_plate = 0.0582e-3 "Volume of the liquid in the cooling plate";
  parameter Modelica.SIunits.HeatFlowRate Q_flow_plate = 300 "Nominal cooling capacity of the cooling plate";
  IBPSA.Fluid.HeatExchangers.HeaterCooler_u plate1_1(redeclare package Medium = Medium, Q_flow_nominal = Q_flow_plate, allowFlowReversal = true, dp_nominal = dp_plate, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, m_flow_nominal = m_flow_plate, tau = V_plate * 1785.9 / m_flow_plate) annotation(
    Placement(visible = true, transformation(origin = {-60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  IBPSA.Fluid.HeatExchangers.HeaterCooler_u plate1_2(redeclare package Medium = Medium, Q_flow_nominal = Q_flow_plate, allowFlowReversal = true,  dp_nominal = dp_plate, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, m_flow_nominal = m_flow_plate, tau = V_plate * 1785.9 / m_flow_plate) annotation(
    Placement(visible = true, transformation(origin = {-20, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  IBPSA.Fluid.HeatExchangers.HeaterCooler_u plate1_3(redeclare package Medium = Medium, Q_flow_nominal = Q_flow_plate, allowFlowReversal = true,  dp_nominal = dp_plate, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, m_flow_nominal = m_flow_plate, tau = V_plate * 1785.9 / m_flow_plate) annotation(
    Placement(visible = true, transformation(origin = {20, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  IBPSA.Fluid.HeatExchangers.HeaterCooler_u plate1_4(redeclare package Medium = Medium, Q_flow_nominal = Q_flow_plate, allowFlowReversal = true,  dp_nominal = dp_plate, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, m_flow_nominal = m_flow_plate, tau = V_plate * 1785.9 / m_flow_plate) annotation(
    Placement(visible = true, transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 180), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput u(min = 0, max = 1) "Control input for cooling plates" annotation(
    Placement(visible = true, transformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput u1 annotation(
    Placement(visible = true, transformation(origin = {120, 80}, extent = {{-20, -20}, {20, 20}}, rotation = 180), iconTransformation(origin = {120, 100}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
  Modelica.Fluid.Fittings.GenericResistances.VolumeFlowRate sidePanel1(redeclare package Medium = Medium, a = 7485597e8, allowFlowReversal = false, b = 1212001e3) annotation(
    Placement(visible = true, transformation(origin = {30, -82}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealInput u2 annotation(
    Placement(visible = true, transformation(origin = {120, 40}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
  Modelica.Blocks.Interfaces.RealInput u3 annotation(
    Placement(visible = true, transformation(origin = {120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
  Modelica.Blocks.Interfaces.RealInput u4 annotation(
    Placement(visible = true, transformation(origin = {120, -40}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
  Modelica.Blocks.Interfaces.RealInput u5 annotation(
    Placement(visible = true, transformation(origin = {120, -78}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
  IBPSA.Fluid.Actuators.Valves.TwoWayLinear val1(redeclare package Medium = Medium, CvData = IBPSA.Fluid.Types.CvTypes.Kv, Kv = 6.3, l = 0.009, m_flow_nominal = 0.1, riseTime = 5)  annotation(
    Placement(visible = true, transformation(origin = {-60, 32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  IBPSA.Fluid.Actuators.Valves.TwoWayLinear val2(redeclare package Medium = Medium, CvData = IBPSA.Fluid.Types.CvTypes.Kv, Kv = 6.3, m_flow_nominal = 0.1, riseTime = 5)  annotation(
    Placement(visible = true, transformation(origin = {-20, 32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  IBPSA.Fluid.Actuators.Valves.TwoWayLinear val3(redeclare package Medium = Medium, CvData = IBPSA.Fluid.Types.CvTypes.Kv, Kv = 6.3, m_flow_nominal = 0.1, riseTime = 5)  annotation(
    Placement(visible = true, transformation(origin = {20, 32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  IBPSA.Fluid.Actuators.Valves.TwoWayLinear val4(redeclare package Medium = Medium, CvData = IBPSA.Fluid.Types.CvTypes.Kv, Kv = 6.3, m_flow_nominal = 0.1, riseTime = 5)  annotation(
    Placement(visible = true, transformation(origin = {60, 32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  IBPSA.Fluid.Actuators.Valves.TwoWayLinear val5(redeclare package Medium = Medium, CvData = IBPSA.Fluid.Types.CvTypes.Kv, Kv = 6.3, m_flow_nominal = 0.1, riseTime = 5)  annotation(
    Placement(visible = true, transformation(origin = {30, -56}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
equation
  connect(plate1_2.port_b, port_b) annotation(
    Line(points = {{-20, -10}, {-20, -32}, {0, -32}, {0, -100}}, color = {0, 127, 255}));
  connect(plate1_3.port_b, port_b) annotation(
    Line(points = {{20, -10}, {20, -32}, {0, -32}, {0, -100}}, color = {0, 127, 255}));
  connect(plate1_4.port_b, port_b) annotation(
    Line(points = {{60, -10}, {60, -32}, {0, -32}, {0, -100}}, color = {0, 127, 255}));
  connect(plate1_4.u, u) annotation(
    Line(points = {{66, 12}, {66, 60}, {-120, 60}}, color = {0, 0, 127}));
  connect(sidePanel1.port_b, port_b) annotation(
    Line(points = {{30, -92}, {30, -100}, {0, -100}}, color = {0, 127, 255}));
  connect(plate1_1.port_b, port_b) annotation(
    Line(points = {{-60, -10}, {-60, -32}, {0, -32}, {0, -100}}, color = {0, 127, 255}));
  connect(plate1_1.u, u) annotation(
    Line(points = {{-54, 12}, {-54, 60}, {-120, 60}}, color = {0, 0, 127}));
  connect(plate1_2.u, u) annotation(
    Line(points = {{-14, 12}, {-14, 60}, {-120, 60}}, color = {0, 0, 127}));
  connect(plate1_3.u, u) annotation(
    Line(points = {{26, 12}, {26, 60}, {-120, 60}}, color = {0, 0, 127}));
  connect(plate1_1.port_a, val1.port_b) annotation(
    Line(points = {{-60, 10}, {-60, 22}}, color = {0, 127, 255}));
  connect(plate1_2.port_a, val2.port_b) annotation(
    Line(points = {{-20, 10}, {-20, 22}}, color = {0, 127, 255}));
  connect(plate1_3.port_a, val3.port_b) annotation(
    Line(points = {{20, 10}, {20, 22}}, color = {0, 127, 255}));
  connect(plate1_4.port_a, val4.port_b) annotation(
    Line(points = {{60, 10}, {60, 22}}, color = {0, 127, 255}));
  connect(val4.y, u4) annotation(
    Line(points = {{72, 32}, {76, 32}, {76, -40}, {120, -40}}, color = {0, 0, 127}));
  connect(val3.y, u3) annotation(
    Line(points = {{32, 32}, {42, 32}, {42, 46}, {82, 46}, {82, 0}, {120, 0}}, color = {0, 0, 127}));
  connect(val2.y, u2) annotation(
    Line(points = {{-8, 32}, {2, 32}, {2, 48}, {90, 48}, {90, 40}, {120, 40}}, color = {0, 0, 127}));
  connect(val1.y, u1) annotation(
    Line(points = {{-48, 32}, {-40, 32}, {-40, 54}, {86, 54}, {86, 80}, {120, 80}}, color = {0, 0, 127}));
  connect(val1.port_a, port_a) annotation(
    Line(points = {{-60, 42}, {-60, 74}, {0, 74}, {0, 100}}, color = {0, 127, 255}));
  connect(val2.port_a, port_a) annotation(
    Line(points = {{-20, 42}, {-20, 66}, {0, 66}, {0, 100}}, color = {0, 127, 255}));
  connect(val3.port_a, port_a) annotation(
    Line(points = {{20, 42}, {20, 66}, {0, 66}, {0, 100}}, color = {0, 127, 255}));
  connect(val4.port_a, port_a) annotation(
    Line(points = {{60, 42}, {58, 42}, {58, 78}, {0, 78}, {0, 100}}, color = {0, 127, 255}));
  connect(sidePanel1.port_a, val5.port_b) annotation(
    Line(points = {{30, -72}, {30, -66}}, color = {0, 127, 255}));
  connect(val5.port_a, port_a) annotation(
    Line(points = {{30, -46}, {30, -40}, {-70, -40}, {-70, 82}, {0, 82}, {0, 100}}, color = {0, 127, 255}));
  connect(val5.y, u5) annotation(
    Line(points = {{42, -56}, {80, -56}, {80, -82}, {120, -82}, {120, -78}}, color = {0, 0, 127}));
  annotation(
    uses(IBPSA(version = "3.0.0"), Modelica(version = "3.2.3")),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Rectangle(origin = {0, 65}, fillColor = {132, 132, 132}, fillPattern = FillPattern.Solid, extent = {{-34, 15}, {34, -15}}), Rectangle(origin = {0, 23}, fillColor = {132, 132, 132}, fillPattern = FillPattern.Solid, extent = {{-34, 15}, {34, -15}}), Rectangle(origin = {0, -21}, fillColor = {132, 132, 132}, fillPattern = FillPattern.Solid, extent = {{-34, 15}, {34, -15}}), Rectangle(origin = {0, -63}, fillColor = {132, 132, 132}, fillPattern = FillPattern.Solid, extent = {{-34, 15}, {34, -15}}), Line(origin = {49.4832, 74.5866}, points = {{-15, 0}, {15, 0}}), Line(origin = {48.4832, 32.5866}, points = {{-14, 0}, {16, 0}}), Line(origin = {47.4832, -9.41341}, points = {{-13, 0}, {17, 0}}), Line(origin = {47.4832, -53.4134}, points = {{-13, 0}, {17, 0}}), Line(origin = {43.4832, 58.5866}, points = {{-9, 0}, {9, 0}}), Line(origin = {43.4832, 16.5866}, points = {{-9, 0}, {9, 0}}), Line(origin = {42.4832, -29.4134}, points = {{-8, 0}, {10, 0}}), Line(origin = {43.4832, -71.4134}, points = {{-9, 0}, {9, 0}}), Line(origin = {52.4832, -6.41341}, points = {{0, 65}, {0, -65}}), Line(origin = {64.4832, 10.5866}, points = {{0, 64}, {0, -64}}), Rectangle(origin = {-49, 0}, fillColor = {122, 122, 122}, fillPattern = FillPattern.Solid, extent = {{-5, 80}, {5, -80}}), Text(origin = {-82, 60}, extent = {{-10, 10}, {10, -10}}, textString = "Q")}),
  Documentation(info = "<html><head></head><body>This model does not include thermal performance of the sice panel</body></html>"));
end thermal_demonstrator;
