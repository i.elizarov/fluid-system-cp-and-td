function Odd_even "Returns true if the argument is even; otherwise, returns false"

  input Integer x;
  output Boolean result;


algorithm

  result := if rem(x, 2) == 0 then true else false;

end Odd_even;
