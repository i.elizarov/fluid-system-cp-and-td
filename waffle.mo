model waffle

replaceable package Medium =
      Modelica.Media.Water.ConstantPropertyLiquidWater;
      
final parameter Modelica.SIunits.Length channel_pair_height = 29e-3 "Height of the first and last block of channels";

parameter Modelica.SIunits.Area channel_cross_section_area = 4.6e-6 "Cross-section area of channels";
parameter Modelica.SIunits.Length channel_cross_section_perimeter = 15.4e-3 "Perimeter of the channels";

parameter Modelica.SIunits.Height channel_roughness = 0.015e-3 "Rougness of wet surface";
final parameter Modelica.SIunits.Length channel_horizontal_length = 29e-3 "Horizonral length of the channel (HLRL)";
final parameter Modelica.SIunits.Length channel_angled_length = 33e-3 "Tilted channel length";
parameter Integer channel_number = 22 "Vertical array of channels";

// Local hydraulic resistance coefficients

final parameter Real epsilon_left_right(min = 0)= 0.5 "Local hydraulic resistance coefficient for channels";
final parameter Real epsilon_left_right_middle(min = 0)= 0.5 "Local hydraulic resistance coefficient for channels";

final parameter Real epsilon_bend(min = 0) = 0.25 "Bend resistance coefficient";
final parameter Real epsilon_joint_stream(min = 0) = 2.0 "Junction resistance coefficient"; 

  Piping_local Array_channels(channel_cross_section_area = channel_cross_section_area,
   channel_cross_section_perimeter = channel_cross_section_perimeter,
   channel_roughness = channel_roughness,
   epsilon_left_right = epsilon_left_right,
   epsilon_left_right_middle = epsilon_left_right_middle,
   redeclare package Medium = Medium,
   channel_number = channel_number) annotation(
    Placement(visible = true, transformation(origin = {2, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  Modelica.Fluid.Pipes.StaticPipe pipe_left(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area,
    allowFlowReversal = false,
    isCircular = false,
    length = channel_pair_height,

    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {-70, 60}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  
  Modelica.Fluid.Pipes.StaticPipe pipe_left_middle(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    length = channel_pair_height,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {-28, 60}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  
  Modelica.Fluid.Pipes.StaticPipe pipe_right_middle(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    length = channel_pair_height,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {32, 60}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe_right(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    length = channel_pair_height,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {70, 60}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_left(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium,
  zeta = epsilon_left_right) annotation(
    Placement(visible = true, transformation(origin = {-70, 28}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_left_middle(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_left_right_middle, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-28, 28}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_right_middle(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_left_right_middle, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {32, 28}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_right(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_left_right, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {70, 30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
 Modelica.Fluid.Pipes.StaticPipe pipe_left_bottom(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    length = channel_pair_height,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {-70, -70}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe_left_middle_bottom(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    length = channel_pair_height,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {-28, -70}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe_right_middle_bottom(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    length = channel_pair_height,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {32, -70}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe_right_bottom(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    length = channel_pair_height,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {70, -70}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_left_bottom(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_left_right, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-70, -38}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_left_middle_bottom(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_left_right_middle, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-28, -38}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_right_middle_bottom(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_left_right_middle, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {32, -38}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_right_bottom(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_left_right, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {70, -36}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  
  
  
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_joint_bottom(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_joint_stream, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-28, -112}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_bend_bottom(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_bend, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-69, -90}, extent = {{-7, -6}, {7, 6}}, rotation = -90)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe_joint_bottom(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness,
    length = channel_horizontal_length) annotation(
    Placement(visible = true, transformation(origin = {-51, -100}, extent = {{-7, -6}, {7, 6}}, rotation = 0)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe_angled_bottom(length = channel_angled_length,
  redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {-28, -132}, extent = {{-8, -8}, {8, 8}}, rotation = -90)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_joint_stream, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {29, -114}, extent = {{-7, -6}, {7, 6}}, rotation = -90)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice1(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_bend, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {70, -98}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness,
    length = channel_horizontal_length) annotation(
    Placement(visible = true, transformation(origin = {51, -110}, extent = {{-7, -6}, {7, 6}}, rotation = 180)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe1(length = channel_angled_length,
  redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {30, -136}, extent = {{-8, -8}, {8, 8}}, rotation = -90)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice2(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_joint_stream, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {0, -164}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
 inner Modelica.Fluid.System system annotation(
    Placement(visible = true, transformation(origin = {110, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

 Modelica.Fluid.Pipes.StaticPipe pipe_joint_top(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness,
    length = channel_horizontal_length) annotation(
    Placement(visible = true, transformation(origin = {-52, 80}, extent = {{-8, -6}, {8, 6}}, rotation = 180)));
 
 Modelica.Fluid.Fittings.SimpleGenericOrifice orrifice_bend_top_1(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_bend, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-71, 79}, extent = {{-5, -5}, {5, 5}}, rotation = 180)));
 
 Modelica.Fluid.Fittings.SimpleGenericOrifice orrifice_joint_top(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_joint_stream, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {-26, 97}, extent = {{-7, -6}, {7, 6}}, rotation = -90)));
 
 Modelica.Fluid.Fittings.SimpleGenericOrifice orifice_bend_top_2(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_bend, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {59, 83}, extent = {{-5, -5}, {5, 5}}, rotation = 0)));

 Modelica.Fluid.Pipes.StaticPipe pipe_joint_top2(redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness,
    length = channel_horizontal_length) annotation(
    Placement(visible = true, transformation(origin = {44, 84}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
 
 Modelica.Fluid.Fittings.SimpleGenericOrifice orifice3(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  zeta = epsilon_joint_stream, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {34, 99}, extent = {{-7, -6}, {7, 6}}, rotation = -90)));
 
 Modelica.Fluid.Pipes.StaticPipe pipe_inclined_top(length = channel_angled_length,
  redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {-25, 126}, extent = {{-8, -7}, {8, 7}}, rotation = -90)));
 
 Modelica.Fluid.Pipes.StaticPipe pipe2(length = channel_angled_length,
  redeclare package Medium = Medium,
    allowFlowReversal = false,
    crossArea = channel_cross_section_area,
    isCircular = false,
    perimeter = channel_cross_section_perimeter,
    roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {35, 124}, extent = {{-8, -7}, {8, 7}}, rotation = -90)));
 
 Modelica.Fluid.Fittings.SimpleGenericOrifice orifice4(diameter = sqrt(4 * channel_cross_section_area / 3.14), zeta = epsilon_joint_stream, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {0, 152}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
 Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {0, 174}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {0, -194}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation
  connect(pipe_left.port_b, orifice_left.port_a) annotation(
    Line(points = {{-70, 50}, {-70, 38}}, color = {0, 127, 255}));
  connect(pipe_left_middle.port_b, orifice_left_middle.port_a) annotation(
    Line(points = {{-28, 50}, {-28, 38}}, color = {0, 127, 255}));
  connect(pipe_right_middle.port_b, orifice_right_middle.port_a) annotation(
    Line(points = {{32, 50}, {32, 38}}));
  connect(pipe_right.port_b, orifice_right.port_a) annotation(
    Line(points = {{70, 50}, {70, 40}}, color = {0, 127, 255}));
  connect(orifice_left_middle.port_b, Array_channels.port_a2) annotation(
    Line(points = {{-28, 18}, {-28, 16}, {0, 16}, {0, 4}}, color = {0, 127, 255}));
  connect(orifice_left.port_b, Array_channels.port_a1) annotation(
    Line(points = {{-70, 18}, {-70, 10}, {-6, 10}, {-6, 4}}, color = {0, 127, 255}));
  connect(orifice_right_middle.port_b, Array_channels.port_a3) annotation(
    Line(points = {{32, 18}, {32, 16}, {6, 16}, {6, 4}}, color = {0, 127, 255}));
  connect(orifice_right.port_b, Array_channels.port_a4) annotation(
    Line(points = {{70, 20}, {70, 10}, {10, 10}, {10, 4}}, color = {0, 127, 255}));
  connect(orifice_left_bottom.port_b, pipe_left_bottom.port_a) annotation(
    Line(points = {{-70, -48}, {-70, -60}}, color = {0, 127, 255}));
  connect(orifice_left_middle_bottom.port_b, pipe_left_middle_bottom.port_a) annotation(
    Line(points = {{-28, -48}, {-28, -60}}, color = {0, 127, 255}));
  connect(orifice_right_middle_bottom.port_b, pipe_right_middle_bottom.port_a) annotation(
    Line(points = {{32, -48}, {32, -60}}, color = {0, 127, 255}));
  connect(orifice_right_bottom.port_b, pipe_right_bottom.port_a) annotation(
    Line(points = {{70, -46}, {70, -60}}, color = {0, 127, 255}));
  connect(orifice_left_middle_bottom.port_a, Array_channels.port_b2) annotation(
    Line(points = {{-28, -28}, {-28, -24}, {-2, -24}, {-2, -16}}, color = {0, 127, 255}));
  connect(Array_channels.port_b1, orifice_left_bottom.port_a) annotation(
    Line(points = {{-6, -16}, {-6, -22}, {-70, -22}, {-70, -28}}, color = {0, 127, 255}));
  connect(Array_channels.port_b3, orifice_right_middle_bottom.port_a) annotation(
    Line(points = {{6, -16}, {6, -24}, {32, -24}, {32, -28}}, color = {0, 127, 255}));
  connect(Array_channels.port_b4, orifice_right_bottom.port_a) annotation(
    Line(points = {{10, -16}, {10, -18}, {70, -18}, {70, -26}}, color = {0, 127, 255}));
  connect(orifice_joint_bottom.port_a, pipe_left_middle_bottom.port_b) annotation(
    Line(points = {{-28, -106}, {-28, -80}}, color = {0, 127, 255}));
  connect(pipe_left_bottom.port_b, orifice_bend_bottom.port_a) annotation(
    Line(points = {{-70, -80}, {-70, -84}, {-69, -84}, {-69, -83}}, color = {0, 127, 255}));
  connect(orifice_bend_bottom.port_b, pipe_joint_bottom.port_a) annotation(
    Line(points = {{-69, -97}, {-69, -100}, {-58, -100}}));
  connect(pipe_right_middle_bottom.port_b, orifice.port_a) annotation(
    Line(points = {{32, -80}, {32, -85}, {29, -85}, {29, -107}}, color = {0, 127, 255}));
  connect(pipe_right_bottom.port_b, orifice1.port_a) annotation(
    Line(points = {{70, -80}, {70, -92}}, color = {0, 127, 255}));
  connect(pipe_angled_bottom.port_b, pipe1.port_b) annotation(
    Line(points = {{-28, -140}, {-28, -144}, {30, -144}}, color = {0, 127, 255}));
  connect(orifice2.port_a, pipe1.port_b) annotation(
    Line(points = {{0, -158}, {0, -144}, {30, -144}}, color = {0, 127, 255}));
  connect(pipe_angled_bottom.port_a, orifice_joint_bottom.port_b) annotation(
    Line(points = {{-28, -124}, {-28, -118}}, color = {0, 127, 255}));
  connect(pipe_joint_bottom.port_b, orifice_joint_bottom.port_a) annotation(
    Line(points = {{-44, -100}, {-28, -100}, {-28, -106}}, color = {0, 127, 255}));
  connect(pipe1.port_a, orifice.port_b) annotation(
    Line(points = {{30, -128}, {30, -120}}, color = {0, 127, 255}));
  connect(orifice1.port_b, pipe.port_a) annotation(
    Line(points = {{70, -104}, {70, -110}, {58, -110}}, color = {0, 127, 255}));
  connect(pipe.port_b, orifice.port_a) annotation(
    Line(points = {{44, -110}, {40, -110}, {40, -104}, {30, -104}, {30, -106}}, color = {0, 127, 255}));
  connect(pipe_joint_top.port_b, orrifice_bend_top_1.port_a) annotation(
    Line(points = {{-60, 80}, {-66, 80}}));
  connect(orrifice_bend_top_1.port_b, pipe_left.port_a) annotation(
    Line(points = {{-76, 80}, {-84, 80}, {-84, 70}, {-70, 70}}, color = {0, 127, 255}));
  connect(pipe_joint_top.port_a, orrifice_joint_top.port_b) annotation(
    Line(points = {{-44, 80}, {-26, 80}, {-26, 90}}, color = {0, 127, 255}));
  connect(pipe_left_middle.port_a, orrifice_joint_top.port_b) annotation(
    Line(points = {{-28, 70}, {-26, 70}, {-26, 90}}, color = {0, 127, 255}));
  connect(orifice_bend_top_2.port_b, pipe_right.port_a) annotation(
    Line(points = {{64, 83}, {70, 83}, {70, 70}}, color = {0, 127, 255}));
  connect(pipe_joint_top2.port_b, orifice_bend_top_2.port_a) annotation(
    Line(points = {{50, 84}, {54, 84}}, color = {0, 127, 255}));
  connect(pipe_joint_top2.port_a, pipe_right_middle.port_a) annotation(
    Line(points = {{38, 84}, {32, 84}, {32, 70}}, color = {0, 127, 255}));
  connect(orifice3.port_b, pipe_right_middle.port_a) annotation(
    Line(points = {{34, 92}, {32, 92}, {32, 70}}, color = {0, 127, 255}));
  connect(pipe_inclined_top.port_b, orrifice_joint_top.port_a) annotation(
    Line(points = {{-24, 118}, {-26, 118}, {-26, 104}}, color = {0, 127, 255}));
  connect(pipe2.port_b, orifice3.port_a) annotation(
    Line(points = {{36, 116}, {34, 116}, {34, 106}}, color = {0, 127, 255}));
  connect(orifice4.port_b, pipe_inclined_top.port_a) annotation(
    Line(points = {{0, 146}, {-2, 146}, {-2, 140}, {-24, 140}, {-24, 134}}, color = {0, 127, 255}));
  connect(pipe2.port_a, orifice4.port_b) annotation(
    Line(points = {{36, 132}, {34, 132}, {34, 140}, {0, 140}, {0, 146}}, color = {0, 127, 255}));
  connect(port_a, orifice4.port_a) annotation(
    Line(points = {{0, 174}, {0, 158}}));
 connect(orifice2.port_b, port_b) annotation(
    Line(points = {{0, -170}, {0, -194}}, color = {0, 127, 255}));
  annotation(
    uses(Modelica(version = "3.2.3")),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Rectangle(origin = {0, -3}, extent = {{-22, 49}, {22, -49}}), Line(origin = {-8, -3}, points = {{0, 49}, {0, -49}, {0, -49}}), Line(origin = {8, -3}, points = {{0, 49}, {0, -49}}), Line(origin = {0, 34}, points = {{-22, 0}, {22, 0}}), Line(origin = {0, 16}, points = {{-22, 0}, {22, 0}}), Line(points = {{-22, 0}, {22, 0}}), Line(origin = {0, -14}, points = {{-22, 0}, {22, 0}}), Line(origin = {0, -26}, points = {{-22, 0}, {22, 0}}), Line(origin = {0, -38}, points = {{-22, 0}, {22, 0}}), Line(origin = {0, 53}, points = {{0, -7}, {0, 7}}), Line(origin = {0, -60}, points = {{0, 8}, {0, -8}})}));
end waffle;
