model SidePanel

replaceable package Medium =
      Novec649;
      
parameter Modelica.SIunits.Area channel_cross_section_area = 4.6e-6 "Cross-section area of channels";
parameter Modelica.SIunits.Length channel_cross_section_perimeter = 15.4e-3 "Perimeter of the channels";
parameter Modelica.SIunits.Height channel_roughness = 0.015e-3 "Rougness of wet surface";


  waffle waffle_1(final channel_number = 22,
   redeclare package Medium = Medium,
    final channel_cross_section_area = channel_cross_section_area,
    final channel_cross_section_perimeter = channel_cross_section_perimeter,
    final channel_roughness = channel_roughness) annotation(
    Placement(visible = true, transformation(origin = {-70, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 
  waffle waffle_2(final channel_number = 20,
   redeclare package Medium = Medium,
    final channel_cross_section_area = channel_cross_section_area,
    final channel_cross_section_perimeter = channel_cross_section_perimeter,
    final channel_roughness = channel_roughness)  annotation(
    Placement(visible = true, transformation(origin = {0, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 
  waffle waffle_3(final channel_number = 20,
   redeclare package Medium = Medium,
    final channel_cross_section_area = channel_cross_section_area,
    final channel_cross_section_perimeter = channel_cross_section_perimeter)  annotation(
    Placement(visible = true, transformation(origin = {70, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe1(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = (65 + 29 / 2 + 40 + 65 + 35) * 1e-3)  annotation(
    Placement(visible = true, transformation(origin = {-52, 62}, extent = {{-6, -6}, {6, 6}}, rotation = 180)));
 
  Modelica.Fluid.Pipes.StaticPipe pipe2(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = 75e-3 + 45e-3)  annotation(
    Placement(visible = true, transformation(origin = {50, 62}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
 
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice1(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium, allowFlowReversal = false, zeta = 0.25)  annotation(
    Placement(visible = true, transformation(origin = {-70, 42}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium, allowFlowReversal = false, zeta = 0.25) annotation(
    Placement(visible = true, transformation(origin = {-70, -22}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  
  Modelica.Fluid.Pipes.StaticPipe pipe3(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = (65 + 29 / 2 + 40 + 65 + 35) * 1e-3)  annotation(
    Placement(visible = true, transformation(origin = {-51, -44}, extent = {{-7, -6}, {7, 6}}, rotation = 0)));
  Modelica.Fluid.Pipes.StaticPipe pipe4(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, length = 75e-3 + 45e-3)  annotation(
    Placement(visible = true, transformation(origin = {49, -44}, extent = {{-7, -6}, {7, 6}}, rotation = 180)));
  
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice2(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium, allowFlowReversal = false, zeta = 0.25) annotation(
    Placement(visible = true, transformation(origin = {70, 38}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice3(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium, allowFlowReversal = false, zeta = 0.25) annotation(
    Placement(visible = true, transformation(origin = {70, -22}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Pipes.StaticPipe pipe(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = 45e-3) annotation(
    Placement(visible = true, transformation(origin = {0, 32}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Pipes.StaticPipe pipe5(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = 45e-3) annotation(
    Placement(visible = true, transformation(origin = {0, -16}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice4(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium, allowFlowReversal = false, zeta = 2 + 0.25) annotation(
    Placement(visible = true, transformation(origin = {-20, 68}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice5(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium, allowFlowReversal = false, zeta = 2 + 0.25) annotation(
    Placement(visible = true, transformation(origin = {-22, -50}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Pipes.StaticPipe pipe6(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = 65e-3) annotation(
    Placement(visible = true, transformation(origin = {6, 78}, extent = {{-6, -6}, {6, 6}}, rotation = 180)));
  Modelica.Fluid.Pipes.StaticPipe pipe7(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = 65e-3) annotation(
    Placement(visible = true, transformation(origin = {-4, -64}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice6(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium, allowFlowReversal = false, zeta = 2 + 0.5) annotation(
    Placement(visible = true, transformation(origin = {26, 84}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice7(diameter = sqrt(4 * channel_cross_section_area / 3.14),
  redeclare package Medium = Medium, allowFlowReversal = false, zeta = 2 + 0.5) annotation(
    Placement(visible = true, transformation(origin = {26, -70}, extent = {{-6, -6}, {6, 6}}, rotation = -90)));
  Modelica.Fluid.Pipes.StaticPipe pipe8(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = 135e-3) annotation(
    Placement(visible = true, transformation(origin = {48, 94}, extent = {{-6, -6}, {6, 6}}, rotation = 180)));
  Modelica.Fluid.Pipes.StaticPipe pipe9(redeclare package Medium = Medium,
    perimeter = channel_cross_section_perimeter,
   crossArea = channel_cross_section_area, allowFlowReversal = false, isCircular = false, length = 75e-3 + 45e-3) annotation(
    Placement(visible = true, transformation(origin = {48, -80}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {88, 94}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {88, -80}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 inner Modelica.Fluid.System system annotation(
    Placement(visible = true, transformation(origin = {-70, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(pipe1.port_b, orifice1.port_a) annotation(
    Line(points = {{-58, 62}, {-70, 62}, {-70, 48}}, color = {0, 127, 255}));
  connect(orifice1.port_b, waffle_1.port_a) annotation(
    Line(points = {{-70, 36}, {-70, 20}}, color = {0, 127, 255}));
  connect(waffle_1.port_b, orifice.port_a) annotation(
    Line(points = {{-70, 0}, {-70, -16}}, color = {0, 127, 255}));
  connect(orifice.port_b, pipe3.port_a) annotation(
    Line(points = {{-70, -28}, {-70, -44}, {-58, -44}}, color = {0, 127, 255}));
  connect(orifice2.port_b, waffle_3.port_a) annotation(
    Line(points = {{70, 32}, {70, 20}}));
  connect(orifice2.port_a, pipe2.port_b) annotation(
    Line(points = {{70, 44}, {70, 62}, {56, 62}}, color = {0, 127, 255}));
  connect(waffle_3.port_b, orifice3.port_a) annotation(
    Line(points = {{70, 0}, {70, -16}}, color = {0, 127, 255}));
  connect(orifice3.port_b, pipe4.port_a) annotation(
    Line(points = {{70, -28}, {70, -44}, {56, -44}}, color = {0, 127, 255}));
  connect(pipe.port_b, waffle_2.port_a) annotation(
    Line(points = {{0, 26}, {0, 20}}, color = {0, 127, 255}));
  connect(waffle_2.port_b, pipe5.port_a) annotation(
    Line(points = {{0, 0}, {0, -10}}, color = {0, 127, 255}));
  connect(pipe1.port_a, orifice4.port_b) annotation(
    Line(points = {{-46, 62}, {-20, 62}}, color = {0, 127, 255}));
  connect(pipe.port_a, orifice4.port_b) annotation(
    Line(points = {{0, 38}, {0, 62}, {-20, 62}}, color = {0, 127, 255}));
  connect(pipe3.port_b, orifice5.port_a) annotation(
    Line(points = {{-44, -44}, {-22, -44}}, color = {0, 127, 255}));
  connect(pipe5.port_b, orifice5.port_a) annotation(
    Line(points = {{0, -22}, {0, -44}, {-22, -44}}, color = {0, 127, 255}));
  connect(orifice5.port_b, pipe7.port_a) annotation(
    Line(points = {{-22, -56}, {-22, -64}, {-10, -64}}, color = {0, 127, 255}));
  connect(pipe6.port_a, orifice6.port_b) annotation(
    Line(points = {{12, 78}, {26, 78}}, color = {0, 127, 255}));
  connect(pipe2.port_a, orifice6.port_b) annotation(
    Line(points = {{44, 62}, {26, 62}, {26, 78}}, color = {0, 127, 255}));
  connect(pipe7.port_b, orifice7.port_a) annotation(
    Line(points = {{2, -64}, {26, -64}}, color = {0, 127, 255}));
  connect(pipe4.port_b, orifice7.port_a) annotation(
    Line(points = {{42, -44}, {26, -44}, {26, -64}}, color = {0, 127, 255}));
  connect(pipe8.port_b, orifice6.port_a) annotation(
    Line(points = {{42, 94}, {26, 94}, {26, 90}}, color = {0, 127, 255}));
  connect(orifice7.port_b, pipe9.port_a) annotation(
    Line(points = {{26, -76}, {26, -80}, {42, -80}}, color = {0, 127, 255}));
  connect(orifice4.port_a, pipe6.port_b) annotation(
    Line(points = {{-20, 74}, {-20, 78}, {0, 78}}, color = {0, 127, 255}));
  connect(pipe8.port_a, port_a) annotation(
    Line(points = {{54, 94}, {88, 94}}, color = {0, 127, 255}));
  connect(pipe9.port_b, port_b) annotation(
    Line(points = {{54, -80}, {88, -80}}, color = {0, 127, 255}));

annotation(
    uses(Modelica(version = "3.2.3")),
    Icon(graphics = {Rectangle(origin = {-60, 0}, extent = {{-20, 40}, {20, -40}}), Rectangle(extent = {{-20, 40}, {20, -40}}), Rectangle(origin = {60, 0}, extent = {{-20, 40}, {20, -40}}), Line(origin = {-60, 20}, points = {{-20, 0}, {20, 0}}), Line(origin = {-60, 0}, points = {{-20, 0}, {20, 0}}), Line(origin = {-60, -20}, points = {{-20, 0}, {20, 0}}), Line(origin = {-60, 0}, points = {{0, 40}, {0, -40}}), Line(origin = {0, 20}, points = {{-20, 0}, {20, 0}}), Line(points = {{-20, 0}, {20, 0}}), Line(origin = {0, -20}, points = {{-20, 0}, {20, 0}}), Line(points = {{0, 40}, {0, -40}}), Line(origin = {60, 0}, points = {{0, 40}, {0, -40}}), Line(origin = {60, 20}, points = {{-20, 0}, {20, 0}}), Line(origin = {60, 0}, points = {{-20, 0}, {20, 0}}), Line(origin = {60, -20}, points = {{-20, 0}, {20, 0}}), Rectangle(origin = {-60, 50}, extent = {{-20, 10}, {20, -10}}), Line(origin = {-60, 50}, points = {{0, 10}, {0, -10}}), Line(origin = {-30, 75}, points = {{30, 15}, {30, 5}, {-30, 5}, {-30, -15}}), Line(origin = {0, 60}, points = {{0, 20}, {0, -20}}), Line(origin = {30, 50}, points = {{30, -10}, {30, 10}, {-30, 10}}), Line(origin = {0, -66}, points = {{0, 26}, {0, -26}}), Line(origin = {30, -50}, points = {{30, 10}, {30, -10}, {-30, -10}}), Rectangle(origin = {-60, -50}, extent = {{-20, 10}, {20, -10}}), Line(origin = {-60, -50}, points = {{0, 10}, {0, -10}}), Line(origin = {-30, -70}, points = {{-30, 10}, {-30, -10}, {30, -10}})}));
end SidePanel;
