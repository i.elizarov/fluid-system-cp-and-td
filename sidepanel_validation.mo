model sidepanel_validation

  package Medium =
      Novec649;
  inner Modelica.Fluid.System system annotation(
    Placement(visible = true, transformation(origin = {90, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
    Medium.BaseProperties medium;
    Medium.Density d=Medium.density(medium.state);
  
  Modelica.Fluid.Sources.FixedBoundary boundary1(redeclare package Medium = Medium, nPorts = 1, p = 1e5) annotation(
    Placement(visible = true, transformation(origin = {-42, -32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  SidePanel sidePanel1(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Sources.FixedBoundary boundary(redeclare package Medium = Medium, nPorts = 1, p = boundary1.p + 0.5e5) annotation(
    Placement(visible = true, transformation(origin = {-42, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  medium.p = 1.013e5;
  medium.T = 273.15 - 40;
  connect(sidePanel1.port_b, boundary1.ports[1]) annotation(
    Line(points = {{0, -10}, {0, -32}, {-32, -32}}, color = {0, 127, 255}));
  connect(boundary.ports[1], sidePanel1.port_a) annotation(
    Line(points = {{-32, 16}, {0, 16}, {0, 10}}, color = {0, 127, 255}));
  annotation(
    uses(Modelica(version = "3.2.3"), IBPSA(version = "3.0.0")),
    Diagram(graphics = {Text(origin = {70, 50}, extent = {{-28, 6}, {28, -6}}, textString = "m [kg/s]       V [cub.m/s]      dP [Pa]", fontName = "Arial"), Line(origin = {60, 21}, points = {{0, 31}, {0, -1}}), Line(origin = {54.5003, 44.5588}, points = {{-20, 0}, {46, 0}}), Text(origin = {91, 32}, extent = {{-5, 2}, {5, -2}}, textString = "1,32e5"), Text(origin = {49, 31}, extent = {{-9, 3}, {9, -3}}, textString = "0,0209772"), Text(origin = {90, 24}, extent = {{-6, 2}, {6, -2}}, textString = "2,64e5"), Text(origin = {49, 25}, extent = {{-9, 3}, {9, -3}}, textString = "0,03"), Text(origin = {91, 40}, extent = {{-5, 2}, {5, -2}}, textString = "0,5e5"), Text(origin = {49, 41}, extent = {{-9, 3}, {9, -3}}, textString = "0,0127068"), Line(origin = {82, 21}, points = {{0, 31}, {0, -1}}), Text(origin = {71, 40}, extent = {{-9, 2}, {9, -2}}, textString = "7.668558-06"), Text(origin = {72, 32}, extent = {{-10, 4}, {10, -4}}, textString = "1.2659747e-05"), Text(origin = {71, 25}, extent = {{-9, 3}, {9, -3}}, textString = "1.8105009e-05"), Text(origin = {60, 15}, extent = {{-20, 3}, {20, -3}}, textString = "a=748559700000000"), Text(origin = {56, 7}, extent = {{-16, 3}, {16, -3}}, textString = "b = 1212001000")}));
end sidepanel_validation;
