model test_novec
  package Medium = Novec649;
  Medium.BaseProperties medium;

  Medium.DynamicViscosity eta=Medium.dynamicViscosity(medium.state);
  Modelica.SIunits.KinematicViscosity nue=eta/d;
  Medium.ThermalConductivity lambda=Medium.thermalConductivity(medium.state);
  Medium.SpecificEntropy s=Medium.specificEntropy(medium.state);
  Medium.SpecificHeatCapacity cv=Medium.specificHeatCapacityCv(medium.state);
  Medium.SpecificInternalEnergy u=Medium.specificInternalEnergy(medium.state);
  Medium.SpecificInternalEnergy h=Medium.specificEnthalpy(medium.state);
  Medium.Density d=Medium.density(medium.state);
  protected
  constant Modelica.SIunits.Time timeUnit = 1;
  constant Modelica.SIunits.Temperature Ta = 1;
equation
  medium.p = 1.013e5;
  medium.T = -40 + 273.15;
 // medium.T = Medium.T_min + time/timeUnit*Ta;
    annotation (experiment(StopTime=1.01));



end test_novec;
