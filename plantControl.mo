model plantControl

  extends coolingPlant;
  pipelines pipelines1 annotation(
    Placement(visible = true, transformation(origin = {130, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  thermal_demonstrator thermal_demonstrator1 annotation(
    Placement(visible = true, transformation(origin = {180, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const1(k = 1) annotation(
    Placement(visible = true, transformation(origin = {228, -32}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Blocks.Sources.Ramp ramp(duration = 250, height = -1, offset = 1, startTime = 50) annotation(
    Placement(visible = true, transformation(origin = {224, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Blocks.Sources.Constant const(k = 1)  annotation(
    Placement(visible = true, transformation(origin = {132, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const2(k = 273.15 - 40)  annotation(
    Placement(visible = true, transformation(origin = {-128, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const3(k = 0.5)  annotation(
    Placement(visible = true, transformation(origin = {-54, -64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const4(k = 0.2)  annotation(
    Placement(visible = true, transformation(origin = {52, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(const2.y, firstOrder.u) annotation(
    Line(points = {{-117, 28}, {-80, 28}}, color = {0, 0, 127}));
  connect(const3.y, val.y) annotation(
    Line(points = {{-43, -64}, {6, -64}, {6, -38}}, color = {0, 0, 127}));
  connect(pipelines1.port_b, volDUT.port_a) annotation(
    Line(points = {{120, 8}, {112, 8}, {112, 36}, {120, 36}, {120, 56}, {112, 56}, {112, 54}}, color = {0, 127, 255}));
  connect(val.port_2, pipelines1.port_T1_a) annotation(
    Line(points = {{16, -26}, {98, -26}, {98, 16}, {120, 16}}, color = {0, 127, 255}));
  connect(const4.y, pump.y) annotation(
    Line(points = {{63, 90}, {76, 90}, {76, 34}, {58, 34}, {58, 42}}, color = {0, 0, 127}));
  connect(pipelines1.port_T1_b, thermal_demonstrator1.port_a) annotation(
    Line(points = {{140, 6}, {148, 6}, {148, 34}, {180, 34}, {180, 20}}, color = {0, 127, 255}));
  connect(const.y, thermal_demonstrator1.u) annotation(
    Line(points = {{144, 90}, {160, 90}, {160, 16}, {168, 16}}, color = {0, 0, 127}));
  connect(thermal_demonstrator1.port_b, pipelines1.port_T2_a) annotation(
    Line(points = {{180, 0}, {180, -12}, {154, -12}, {154, 16}, {140, 16}}, color = {0, 127, 255}));
  connect(const1.y, thermal_demonstrator1.u2) annotation(
    Line(points = {{217, -32}, {200, -32}, {200, 14}, {192, 14}}, color = {0, 0, 127}));
  connect(thermal_demonstrator1.u3, const1.y) annotation(
    Line(points = {{192, 10}, {200, 10}, {200, -32}, {217, -32}}, color = {0, 0, 127}));
  connect(thermal_demonstrator1.u4, const1.y) annotation(
    Line(points = {{192, 6}, {200, 6}, {200, -32}, {217, -32}}, color = {0, 0, 127}));
  connect(thermal_demonstrator1.u5, const1.y) annotation(
    Line(points = {{192, 2}, {200, 2}, {200, -32}, {217, -32}}, color = {0, 0, 127}));
  connect(ramp.y, thermal_demonstrator1.u1) annotation(
    Line(points = {{214, 50}, {206, 50}, {206, 20}, {192, 20}}, color = {0, 0, 127}));

annotation(
    uses(Modelica(version = "3.2.3")));
end plantControl;
