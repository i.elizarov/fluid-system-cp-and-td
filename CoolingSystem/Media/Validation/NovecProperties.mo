within CoolingSystem.Media.Validation;

model NovecProperties "Model that tests the implementation of the fluid properties"
  extends Modelica.Icons.Example;
  extends CoolingSystem.Media.Validation.BaseClasses.FluidProperties(
    redeclare package Medium = CoolingSystem.Media.ConstantPropertyLiquidNovec,
    TMin=233.15,
    TMax=303.15);
equation
  // Check the implementation of the base properties
  basPro.state.p=p;
  basPro.state.T=T;
  
   annotation(experiment(Tolerance=1e-6, StopTime=1.0));
end NovecProperties;
