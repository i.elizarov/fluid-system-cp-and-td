within CoolingSystem.Media;

package ConstantPropertyLiquidNovec "Novec649: Simple liquid Novec 649 medium (incompressible, constant data)"

  extends Modelica.Icons.VariantsPackage;


  //   redeclare record extends FluidConstants
  //   end FluidConstants;

  constant Modelica.Media.Interfaces.Types.Basic.FluidConstants[1]
    simpleNovecConstants(
    each chemicalFormula="C6F12O",
    each structureFormula="C6F12O",
    each casRegistryNumber="756-13-8",
    each iupacName="Novec649",
    each molarMass=0.31601);

  extends Modelica.Media.Interfaces.PartialSimpleMedium(
    mediumName="SimpleLiquidNovec649",
    cp_const=1093,
    cv_const=1093,
    d_const=1786,
    eta_const=1.970e-3,
    lambda_const=0.07102,
    a_const=715.87,
    T_min=Modelica.SIunits.Conversions.from_degC(-41),
    T_max=Modelica.SIunits.Conversions.from_degC(49),
    T0=273.15,
    MM_const=0.31601,
    fluidConstants=simpleNovecConstants,
    p_default=300000,
    reference_p=300000,
    reference_T=273.15,
    reference_X={1},
    AbsolutePressure(start=p_default),
    Temperature(start=T_default),
    Density(start=d_const)
    //final cv_const=cp_const
    );
    
    redeclare replaceable model BaseProperties "Base properties (p, d, T, h, u, R, MM and X and Xi) of a medium"
    parameter Boolean preferredMediumStates=false
      "= true if StateSelect.prefer shall be used for the independent property variables of the medium"
      annotation (Evaluate=true, Dialog(tab="Advanced"));
    final parameter Boolean standardOrderComponents=true
      "If true, and reducedX = true, the last element of X will be computed from the other ones";
    Modelica.SIunits.Density d=d_const "Density of medium";
    Temperature T(stateSelect=
      if preferredMediumStates then StateSelect.prefer else StateSelect.default)
      "Temperature of medium";
    InputAbsolutePressure p "Absolute pressure of medium";
    InputMassFraction[nXi] Xi=fill(0, 0)
      "Structurally independent mass fractions";
    InputSpecificEnthalpy h "Specific enthalpy of medium";
    Modelica.SIunits.SpecificInternalEnergy u
      "Specific internal energy of medium";

    Modelica.SIunits.MassFraction[nX] X={1}
      "Mass fractions (= (component mass)/total mass  m_i/m)";
    final Modelica.SIunits.SpecificHeatCapacity R=0
      "Gas constant (of mixture if applicable)";
    final Modelica.SIunits.MolarMass MM=MM_const
      "Molar mass (of mixture or single fluid)";
    ThermodynamicState state
      "Thermodynamic state record for optional functions";


    Modelica.SIunits.Conversions.NonSIunits.Temperature_degC T_degC=
        Modelica.SIunits.Conversions.to_degC(T)
      "Temperature of medium in [degC]";
    Modelica.SIunits.Conversions.NonSIunits.Pressure_bar p_bar=
        Modelica.SIunits.Conversions.to_bar(p)
      "Absolute pressure of medium in [bar]";

    // Local connector definition, used for equation balancing check
    connector InputAbsolutePressure = input Modelica.SIunits.AbsolutePressure
      "Pressure as input signal connector";
    connector InputSpecificEnthalpy = input Modelica.SIunits.SpecificEnthalpy
      "Specific enthalpy as input signal connector";
    connector InputMassFraction = input Modelica.SIunits.MassFraction
      "Mass fraction as input signal connector";

  equation
    h = cp_const*(T-reference_T);
    u = h;
    state.T = T;
    state.p = p;

    // Assertions to test for bounds
    assert(noEvent(T >= T_min), "In " + getInstanceName() + ": Temperature T = " + String(T) + " K exceeded its minimum allowed value of " +
  String(T_min-273.15) + " degC (" + String(T_min) + " Kelvin) as required from medium model");

    assert(noEvent(T <= T_max), "In " + getInstanceName() + ": Temperature T = " + String(T) + " K exceeded its maximum allowed value of " +
  String(T_max-273.15) + " degC (" + String(T_max) + " Kelvin) as required from medium model");

    assert(noEvent(p >= 0.0), "Pressure (= " + String(p) + " Pa) of medium is negative\n(Temperature = " + String(T) + " K)");

  end BaseProperties;

function enthalpyOfLiquid "Return the specific enthalpy of liquid"
  extends Modelica.Icons.Function;
  input Modelica.SIunits.Temperature T "Temperature";
  output Modelica.SIunits.SpecificEnthalpy h "Specific enthalpy";
algorithm
  h := cp_const*(T-reference_T);
annotation (
  smoothOrder=5,
  Inline=true);
end enthalpyOfLiquid;

end ConstantPropertyLiquidNovec;
