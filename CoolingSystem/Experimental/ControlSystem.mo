within CoolingSystem.Experimental;
model ControlSystem

 extends CoolingSystem.Devices.CoolingPlant;

 parameter Modelica.SIunits.Time startTime = 100;
 parameter Modelica.SIunits.Time duration = 30;
 parameter Real opening(min = 0, max = 1) = 0.8;
 parameter Modelica.SIunits.Time pause = 20;

  CoolingSystem.Devices.Pipelines pipelines1 annotation (
    Placement(visible = true, transformation(origin = {90, -28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  CoolingSystem.Devices.ThermalDemonstrator thermal_demonstrator1(leakage = 0.05)  annotation (
    Placement(visible = true, transformation(origin = {180, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const3(k = 1)  annotation (
    Placement(visible = true, transformation(origin = {-50, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Sources.Ramp op1(duration = duration, height = opening, startTime = 100)  annotation (
    Placement(visible = true, transformation(origin = {128, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Sources.Ramp op2(duration = duration, height = opening, startTime = op1.startTime + op1.duration + pause)  annotation (
    Placement(visible = true, transformation(origin = {128, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Sources.Ramp op3(duration = duration, height = opening, startTime = op2.startTime + op2.duration + pause)  annotation (
    Placement(visible = true, transformation(origin = {128, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Sources.Ramp op4(duration = duration, height = opening, startTime = op3.startTime + op3.duration + pause)  annotation (
    Placement(visible = true, transformation(origin = {128, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Sources.Ramp op5(duration = duration, height = opening, startTime = op4.startTime + op4.duration + pause)  annotation (
    Placement(visible = true, transformation(origin = {128, -80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Continuous.FirstOrder firstOrder2(T = 60, y_start = 0)  annotation (
    Placement(visible = true, transformation(origin = {236, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Sources.Step step(startTime = op5.startTime + op5.duration + 5 * pause)  annotation (
    Placement(visible = true, transformation(origin = {206, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Sources.Step step1(height = -60, offset = 273.15 + 20, startTime = step.startTime)  annotation (
    Placement(visible = true, transformation(origin = {-150, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 IBPSA.Controls.Continuous.LimPID pumpPID(
    Ti=0.5e2/2,
    controllerType=Modelica.Blocks.Types.SimpleController.PID,
    k=100e-6,
    Td=0.5,                                                                                                                    strict = true, yMin = 0.2,
    y_start=0.2)                                                                                                                                                          annotation (
    Placement(visible = true, transformation(origin={-90,110},    extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 Modelica.Blocks.Sources.Constant setPD(k = 1.5e5)  annotation (
    Placement(visible = true, transformation(origin = {-134, 110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(op1.y, thermal_demonstrator1.u1) annotation (
    Line(points={{139,48},{159,48},{159,-3},{168,-3}},          color = {0, 0, 127}));
  connect(op2.y, thermal_demonstrator1.u2) annotation (
    Line(points={{139,18},{155,18},{155,-7.8},{168,-7.8}},      color = {0, 0, 127}));
  connect(op3.y, thermal_demonstrator1.u3) annotation (
    Line(points={{139,-14},{155,-14},{155,-12.8},{168,-12.8}},      color = {0, 0, 127}));
  connect(op4.y, thermal_demonstrator1.u4) annotation (
    Line(points={{139,-48},{153,-48},{153,-17.6},{168,-17.6}},      color = {0, 0, 127}));
  connect(op5.y, thermal_demonstrator1.u5) annotation (
    Line(points={{139,-80},{157,-80},{157,-22.4},{168,-22.4}},      color = {0, 0, 127}));
 connect(firstOrder2.y, thermal_demonstrator1.u) annotation (
    Line(points = {{247, 30}, {256, 30}, {256, -6}, {192, -6}}, color = {0, 0, 127}));
 connect(step.y, firstOrder2.u) annotation (
    Line(points = {{217, 30}, {224, 30}}, color = {0, 0, 127}));
 connect(pipelines1.port_T2_a, thermal_demonstrator1.port_b) annotation (
    Line(points={{100,-22.8},{106,-22.8},{106,68},{180,68},{180,-2}},        color = {0, 127, 255}));
 connect(thermal_demonstrator1.port_a, pipelines1.port_T1_b) annotation (
    Line(points={{180,-22},{180,-108},{106,-108},{106,-32.6},{100,-32.6}},        color = {0, 127, 255}));
 connect(step1.y, firstOrder.u) annotation (
    Line(points={{-139,8},{-121.6,8}},   color = {0, 0, 127}));
 connect(const3.y, val.y) annotation (
    Line(points={{-39,-70},{-20,-70},{-20,-46}},        color = {0, 0, 127}));
 connect(pipelines1.port_b, senTem_T2.port_a) annotation (
    Line(points = {{80, -23}, {74, -23}, {74, 34}, {50, 34}}, color = {0, 127, 255}));
 connect(pipelines1.port_T1_a, senTem_T1.port_b) annotation (
    Line(points={{80.2,-32.8},{74,-32.8},{74,-34},{50,-34}},    color = {0, 127, 255}));
 connect(pumpPID.y, pump.y) annotation (
    Line(points={{-79,110},{-30,110},{-30,46}},        color = {0, 0, 127}));
 connect(setPD.y, pumpPID.u_s) annotation (
    Line(points={{-123,110},{-102,110}},      color = {0, 0, 127}));
 connect(pumpPID.u_m, senRelPre.p_rel) annotation (
    Line(points={{-90,98},{-90,80},{-42,80},{-42,64},{84,64},{84,2},{69,2}},                color = {0, 0, 127}));
end ControlSystem;
