within CoolingSystem.Devices;

model CoolingPlant "Model of the cooling plant"

  package Medium =
      CoolingSystem.Media.ConstantPropertyLiquidNovec;

  parameter Modelica.SIunits.HeatFlowRate Q_max = 10.83e3 "Maximum cooling capacity of the plant";
    
  IBPSA.Fluid.Movers.SpeedControlled_y pump(
    redeclare package Medium = Medium,
    redeclare CoolingSystem.Data.PBMX201E102 per, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, p_start = 3e5
  
  ) annotation(
    Placement(visible = true, transformation(origin = {-30, 34}, extent = {{-10, 10}, {10, -10}}, rotation = 180)));
  
  IBPSA.Fluid.Sources.Boundary_pT bou(redeclare package Medium = Medium, T = 273.15 + 20, nPorts = 1, p = 3e5)  annotation(
    Placement(visible = true, transformation(origin = {-98, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  inner Modelica.Fluid.System system annotation(
    Placement(visible = true, transformation(origin = {90, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  IBPSA.Fluid.Actuators.Valves.ThreeWayEqualPercentageLinear val(redeclare package Medium = Medium, CvData = IBPSA.Fluid.Types.CvTypes.Kv, Kv = 12, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, l = {0.02e-3, 0.2e-3}, m_flow_nominal = 1.125, m_flow_small = system.m_flow_small, p_start = 3e5, portFlowDirection_1 = Modelica.Fluid.Types.PortFlowDirection.Entering, portFlowDirection_2 = Modelica.Fluid.Types.PortFlowDirection.Leaving, portFlowDirection_3 = Modelica.Fluid.Types.PortFlowDirection.Leaving, riseTime = 15, use_inputFilter = true, y_start = 0.5)  annotation(
    Placement(visible = true, transformation(origin = {-20, -34}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
  IBPSA.Fluid.Sensors.VolumeFlowRate volJun(redeclare package Medium = Medium, allowFlowReversal = false, m_flow_nominal = 1.125 * 0.5)  annotation(
    Placement(visible = true, transformation(origin = {-20, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  IBPSA.Fluid.Sensors.VolumeFlowRate volDUT(redeclare package Medium = Medium, allowFlowReversal = false, m_flow_nominal = 1.125 * 0.5)  annotation(
    Placement(visible = true, transformation(origin = {10, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  IBPSA.Fluid.FixedResistances.PressureDrop filter(redeclare package Medium = Medium, dp_nominal = 0.07e5, m_flow_nominal = 1.5854) annotation(
    Placement(visible = true, transformation(origin = {-64, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  IBPSA.Fluid.HeatExchangers.SensibleCooler_T coo(redeclare package Medium = Medium, QMin_flow = -Q_max, dp_nominal = 7.94e3, energyDynamics = Modelica.Fluid.Types.Dynamics.FixedInitial, m_flow_nominal = 4050 / 3600, tau = 40) annotation(
    Placement(visible = true, transformation(origin = {-92, -10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder(T = 287.61, y_start = 273.15 + 20) annotation(
    Placement(visible = true, transformation(origin = {-112, 8}, extent = {{-8, -8}, {8, 8}}, rotation = 0)));
  IBPSA.Fluid.Sensors.RelativePressure senRelPre(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {60, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  IBPSA.Fluid.Sensors.TemperatureTwoPort senTem_T2(redeclare package Medium = Medium, m_flow_nominal = 1.125 * 0.5)  annotation(
    Placement(visible = true, transformation(origin = {40, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  IBPSA.Fluid.Sensors.TemperatureTwoPort senTem_T1(redeclare package Medium = Medium, m_flow_nominal = 1.125 * 0.5)  annotation(
    Placement(visible = true, transformation(origin = {40, -34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(val.port_3, volJun.port_a) annotation(
    Line(points = {{-20, -24}, {-20, -10}}, color = {0, 127, 255}));
  connect(pump.port_a, volJun.port_b) annotation(
    Line(points = {{-20, 34}, {-20, 10}}, color = {0, 127, 255}));
  connect(volDUT.port_b, pump.port_a) annotation(
    Line(points = {{0, 34}, {-20, 34}}, color = {0, 127, 255}));
  connect(pump.port_b, filter.port_a) annotation(
    Line(points = {{-40, 34}, {-54, 34}}, color = {0, 127, 255}));
  connect(bou.ports[1], filter.port_b) annotation(
    Line(points = {{-88, 50}, {-74, 50}, {-74, 34}}, color = {0, 127, 255}));
  connect(coo.port_a, filter.port_b) annotation(
    Line(points = {{-92, 0}, {-92, 34}, {-74, 34}}, color = {0, 127, 255}));
  connect(coo.port_b, val.port_1) annotation(
    Line(points = {{-92, -20}, {-92, -34}, {-30, -34}}, color = {0, 127, 255}));
  connect(firstOrder.y, coo.TSet) annotation(
    Line(points = {{-103.2, 8}, {-84.4, 8}, {-84.4, 2}}, color = {0, 0, 127}));
  connect(volDUT.port_a, senTem_T2.port_b) annotation(
    Line(points = {{20, 34}, {30, 34}}, color = {0, 127, 255}));
  connect(senTem_T2.port_a, senRelPre.port_b) annotation(
    Line(points = {{50, 34}, {60, 34}, {60, 12}}, color = {0, 127, 255}));
  connect(senTem_T1.port_b, senRelPre.port_a) annotation(
    Line(points = {{50, -34}, {60, -34}, {60, -8}}, color = {0, 127, 255}));
  connect(senTem_T1.port_a, val.port_2) annotation(
    Line(points = {{30, -34}, {-10, -34}}, color = {0, 127, 255}));
  annotation(
    uses(Modelica(version = "3.2.3"), IBPSA(version = "3.0.0")),
    Diagram);


end CoolingPlant;
