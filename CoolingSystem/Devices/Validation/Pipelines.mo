within CoolingSystem.Devices.Validation;

model Pipelines 

 package Medium = CoolingSystem.Media.ConstantPropertyLiquidNovec;
 
 
  inner Modelica.Fluid.System system annotation(
    Placement(visible = true, transformation(origin = {90, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  CoolingSystem.Devices.Pipelines pipelines1 annotation(
    Placement(visible = true, transformation(origin = {8, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Sources.Boundary_pT boundary1(redeclare package Medium = Medium, T = 273.15 - 40, p = 3e5, nPorts = 1)  annotation(
    Placement(visible = true, transformation(origin = {-70, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Sources.Boundary_pT boundary(redeclare package Medium = Medium, T = 273.15 - 40, nPorts = 1, p = 2e5) annotation(
    Placement(visible = true, transformation(origin = {80, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Fluid.Sources.Boundary_pT boundary2(redeclare package Medium = Medium, T = 273.15 - 40, nPorts = 1, p = 2e5) annotation(
    Placement(visible = true, transformation(origin = {76, -26}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Fluid.Sources.Boundary_pT boundary3(redeclare package Medium = Medium, T = 273.15 - 40, nPorts = 1, p = 1e5) annotation(
    Placement(visible = true, transformation(origin = {-68, -38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(boundary1.ports[1], pipelines1.port_T1_a) annotation(
    Line(points = {{-60, 46}, {-24, 46}, {-24, 8}, {-2, 8}}, color = {0, 127, 255}));
  connect(boundary.ports[1], pipelines1.port_T1_b) annotation(
    Line(points = {{70, 40}, {44, 40}, {44, 6}, {18, 6}, {18, 8}}, color = {0, 127, 255}));
  connect(boundary2.ports[1], pipelines1.port_T2_a) annotation(
    Line(points = {{66, -26}, {40, -26}, {40, -2}, {18, -2}}, color = {0, 127, 255}));
  connect(boundary3.ports[1], pipelines1.port_b) annotation(
    Line(points = {{-58, -38}, {-16, -38}, {-16, 2}, {-2, 2}, {-2, 0}}, color = {0, 127, 255}));
  annotation(
    uses(Modelica(version = "3.2.3"), IBPSA(version = "3.0.0")),
    Diagram);



end Pipelines;
