within CoolingSystem.Devices.Validation;

model ThermalDemonstrator "Validation model for the thermal demonstrator"

  package Medium =
      CoolingSystem.Media.ConstantPropertyLiquidNovec;
  inner Modelica.Fluid.System system annotation(
    Placement(visible = true, transformation(origin = {90, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  

  
  Modelica.Fluid.Sources.FixedBoundary boundary1(redeclare package Medium = Medium, nPorts = 1, p = 1e5) annotation(
    Placement(visible = true, transformation(origin = {-48, -72}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Fluid.Sources.FixedBoundary boundary(redeclare package Medium = Medium, nPorts = 1, p = boundary1.p + 1.32e5) annotation(
    Placement(visible = true, transformation(origin = {-40, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  CoolingSystem.Devices.ThermalDemonstrator thermal_demonstrator1(redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {4, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  IBPSA.Fluid.Sensors.TemperatureTwoPort senTem2(redeclare package Medium = Medium, m_flow_nominal = 0.0893455 * 4) annotation(
    Placement(visible = true, transformation(origin = {38, -50}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Sources.Constant const1(k = 1)  annotation(
    Placement(visible = true, transformation(origin = {84, -16}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Blocks.Sources.Ramp ramp(duration = 250, height = -1, offset = 1, startTime = 50)  annotation(
    Placement(visible = true, transformation(origin = {72, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Blocks.Sources.Ramp ramp1(duration = 250, height = -1, offset = 1, startTime = 50) annotation(
    Placement(visible = true, transformation(origin = {-66, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(senTem2.port_b, boundary1.ports[1]) annotation(
    Line(points = {{38, -60}, {38, -72}, {-38, -72}}, color = {0, 127, 255}));
  connect(boundary.ports[1], thermal_demonstrator1.port_a) annotation(
    Line(points = {{-30, 38}, {4, 38}, {4, -2}}, color = {0, 127, 255}));
  connect(thermal_demonstrator1.port_b, senTem2.port_a) annotation(
    Line(points = {{4, -22}, {4, -34}, {38, -34}, {38, -40}}, color = {0, 127, 255}));
  connect(thermal_demonstrator1.u2, const1.y) annotation(
    Line(points = {{16, -8}, {49.5, -8}, {49.5, -16}, {73, -16}}, color = {0, 0, 127}));
  connect(thermal_demonstrator1.u3, const1.y) annotation(
    Line(points = {{16, -12}, {49.5, -12}, {49.5, -16}, {73, -16}}, color = {0, 0, 127}));
  connect(thermal_demonstrator1.u4, const1.y) annotation(
    Line(points = {{16, -16}, {73, -16}}, color = {0, 0, 127}));
  connect(thermal_demonstrator1.u5, const1.y) annotation(
    Line(points = {{16, -20}, {30, -20}, {30, -16}, {73, -16}}, color = {0, 0, 127}));
  connect(ramp.y, thermal_demonstrator1.u1) annotation(
    Line(points = {{62, 40}, {42, 40}, {42, -2}, {16, -2}}, color = {0, 0, 127}));
  connect(ramp1.y, thermal_demonstrator1.u) annotation(
    Line(points = {{-54, 2}, {-24, 2}, {-24, -4}, {-8, -4}, {-8, -6}}, color = {0, 0, 127}));
  annotation(
    uses(Modelica(version = "3.2.3"), IBPSA(version = "3.0.0")),
    Diagram);


end ThermalDemonstrator;
