package Novec649 "NOVEC649 incompressible model"
  extends Modelica.Media.Incompressible.TableBased(
    mediumName="NOVEC649",
    T_min = Modelica.SIunits.Conversions.from_degC(-50), T_max = Modelica.SIunits.Conversions.from_degC(30),
    TinK = false, T0=273.15,
    tableDensity=
      [-50, 1833.65; -40, 1808.54 ; 0, 1708.06; 30, 1631.37],
       
    tableHeatCapacity=
       [-50, 1099; 0, 1091; 30, 1105.4],
    
    // Replace later
    tableConductivity=
       [-50, 0.073; 0, 0.0634; 30, 0.058],

    tableViscosity=
       [-50, 2.547e-3; -40, 1.970e-3; 0, 0.8874e-3; 30, 0.5714e-3],
       
    tableVaporPressure=
       [-50, 407.49; 0, 12365.28; 30, 49354]);
    annotation (Documentation(info="<html>

</html>"));
end Novec649;
