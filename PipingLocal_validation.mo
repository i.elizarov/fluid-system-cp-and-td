model PipingLocal_validation

  package Medium =
      Novec649;
  inner Modelica.Fluid.System system  annotation(
    Placement(visible = true, transformation(origin = {90, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
 
 
parameter Modelica.SIunits.Area channel_cross_section_area = 4.6e-6 "Cross-section area of channels";
parameter Modelica.SIunits.Length channel_cross_section_perimeter = 15.4e-3 "Perimeter of the channels";
 
parameter Modelica.SIunits.Height channel_roughness = 0.015e-3 "Rougness of wet surface";
 
  Modelica.Fluid.Sources.FixedBoundary boundary(redeclare package Medium = Medium, T = 273.15 - 40, nPorts = 2, p = 2e5)  annotation(
    Placement(visible = true, transformation(origin = {-70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  Modelica.Fluid.Sources.FixedBoundary boundary1(redeclare package Medium = Medium, nPorts = 2, p = 1e5) annotation(
    Placement(visible = true, transformation(origin = {-70, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Piping_local piping_local(final channel_cross_section_area = channel_cross_section_area,
  final channel_cross_section_perimeter = channel_cross_section_perimeter,
  channel_number = 2, final channel_roughness = channel_roughness)  annotation(
    Placement(visible = true, transformation(origin = {-22, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  Modelica.Fluid.Pipes.StaticPipe pipe1(allowFlowReversal = false, length = 40e-3, crossArea = channel_cross_section_area, roughness = channel_roughness,
  perimeter = channel_cross_section_perimeter,
    redeclare package Medium=Medium)  annotation(
    Placement(visible = true, transformation(origin = {36, 30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Fluid.Pipes.StaticPipe pipe2(allowFlowReversal = false, length = 40e-3, crossArea = channel_cross_section_area, roughness = channel_roughness,
  perimeter = channel_cross_section_perimeter,
    redeclare package Medium=Medium)  annotation(
    Placement(visible = true, transformation(origin = {36, -36}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Fluid.Fittings.SimpleGenericOrifice orifice(zeta = 0.5, diameter = sqrt(4 * channel_cross_section_area / 3.14), redeclare package Medium=Medium)  annotation(
    Placement(visible = true, transformation(origin = {36, -4}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
 Modelica.Fluid.Sources.FixedBoundary boundary2(T = 273.15 - 40, nPorts = 3, p = 2e5, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {4, 42}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
 Modelica.Fluid.Sources.FixedBoundary boundary3(nPorts = 3, p = 1e5, redeclare package Medium = Medium) annotation(
    Placement(visible = true, transformation(origin = {4, -34}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
equation
  connect(piping_local.port_a1, boundary.ports[1]) annotation(
    Line(points = {{-29, 8}, {-30, 8}, {-30, 70}, {-60, 70}}, color = {0, 127, 255}));
  connect(piping_local.port_b1, boundary1.ports[1]) annotation(
    Line(points = {{-30, -12}, {-30, -50}, {-60, -50}}, color = {0, 127, 255}));
  connect(pipe2.port_b, boundary1.ports[2]) annotation(
    Line(points = {{36, -46}, {36, -50}, {-60, -50}}, color = {0, 127, 255}));
  connect(pipe1.port_b, orifice.port_a) annotation(
    Line(points = {{36, 20}, {36, 6}}, color = {0, 127, 255}));
  connect(orifice.port_b, pipe2.port_a) annotation(
    Line(points = {{36, -14}, {36, -26}}));
  connect(pipe1.port_a, boundary.ports[2]) annotation(
    Line(points = {{36, 40}, {36, 70}, {-60, 70}}, color = {0, 127, 255}));
 connect(boundary2.ports[1], piping_local.port_a2) annotation(
    Line(points = {{-6, 42}, {-24, 42}, {-24, 8}}, color = {0, 127, 255}));
 connect(boundary2.ports[2], piping_local.port_a3) annotation(
    Line(points = {{-6, 42}, {-18, 42}, {-18, 8}}, color = {0, 127, 255}));
 connect(boundary2.ports[3], piping_local.port_a4) annotation(
    Line(points = {{-6, 42}, {-14, 42}, {-14, 8}}, color = {0, 127, 255}));
 connect(boundary3.ports[1], piping_local.port_b4) annotation(
    Line(points = {{-6, -34}, {-14, -34}, {-14, -12}}, color = {0, 127, 255}));
 connect(boundary3.ports[2], piping_local.port_b3) annotation(
    Line(points = {{-6, -34}, {-18, -34}, {-18, -12}}, color = {0, 127, 255}));
 connect(boundary3.ports[3], piping_local.port_b2) annotation(
    Line(points = {{-6, -34}, {-26, -34}, {-26, -12}}, color = {0, 127, 255}));
  annotation(
    uses(Modelica(version = "3.2.3")),
    Diagram);
end PipingLocal_validation;
